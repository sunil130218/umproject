from django.urls import path
from .views import *


app_name = "umapp"
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('signin/', SigninView.as_view(), name='signin'),
    path('signout/', SignoutView.as_view(), name='signout'),
    path('um-admin/', AdminHomeView.as_view(), name='admin'),
    path('um-admin/blog/create', AdminBlogCreateView.as_view(),
         name='adminblogcreate'),
    path('blog/list/', AdminBlogListView.as_view(), name='adminbloglist'),
    path('blog/<int:pk>/upd/', AdminBlogUpdateView.as_view(), name="adminblogupdate"),
    path('blog/<int:pk>/delete/',
         AdminBlogDeleteView.as_view(), name="adminblogdelete"),
    path('blog/<int:pk>/detail/', BlogDetailView.as_view(), name='blogdetail'),
    path('blog/<int:pk>/comment/', BlogCommentView.as_view(), name='blogcomment'),
    path('password-change/', PasswordChangeView.as_view(), name='passwordchange'),


]
