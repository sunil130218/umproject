from django.contrib.auth.mixins import LoginRequiredMixin
# these are defined function import paxi ko
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .forms import *
from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *


class HomeView(TemplateView):
    template_name = 'home.html'


class SignUpView(FormView):
    template_name = 'signup.html'
    form_class = SignupForm
    success_url = '/'

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        email = form.cleaned_data["email"]
        pword = form.cleaned_data["password"]
        print(uname, email, pword)
        User.objects.create_user(uname, email, pword)

        return super().form_valid(form)


class SigninView(FormView):
    template_name = 'signin.html'
    form_class = SigninForm
    success_url = '/'

    def form_valid(self, form):
        a = form.cleaned_data["username"]
        b = form.cleaned_data["password"]
        # print(a,b,"_______________________")
        user = authenticate(username=a, password=b)
        print (user, "**********")

        if user is not None:
            login(self.request, user)

        else:
            return render(self.request, "signin.html", {"error": "Invalid Username Or Password", "form": form})

        return super().form_valid(form)


class SignoutView(View):
    def get(self, request):
        logout(request)

        return redirect("/signin/")


"""
class AdminHomeView(LoginRequiredMixin,TemplateView):
    template_name = 'adminhome.html'
    login_url = '/signin/'
"""


class AdminHomeView(TemplateView):
    template_name = "adminhome.html"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            pass

        else:
            return redirect('/signin/')

        return super().dispatch(request, *args, **kwargs)


class AdminBlogCreateView(LoginRequiredMixin, CreateView):
    template_name = "adminblogcreate.html"
    form_class = BlogForm
    success_url = "/"
    login_url = '/signin/'

    def form_valid(self, form):
        logged_in_user = self.request.user
        form.instance.author = logged_in_user

        return super().form_valid(form)


class AdminBlogListView(ListView):
    template_name = "adminbloglist.html"
    #model= BLog
    queryset = Blog.objects.all().order_by("-id")
    context_object_name = "allblogs"

    def get_queryset(self):
        data = super().get_queryset()

        return data.filter(author=self.request.user)


class AdminBlogUpdateView(UpdateView):
    template_name = "adminblogcreate.html"
    form_class = BlogForm
    model = Blog
    success_url = '/blog/list/'

    def dispatch(self, request, *args, **kwargs):
        blog_id = self.kwargs["pk"]
        print(blog_id, "*****")
        blog = Blog.objects.get(id=blog_id)
        print(blog_id, "__________")
        if blog.author == request.user:
            pass

        else:
            return render(request, "error.html", {"error": "Your Are not authorised to edit this blog, Only the creator can modify this blog"})

        return super().dispatch(request, *args, **kwargs)


class AdminBlogDeleteView(DeleteView):
    template_name = "adminblogdelete.html"
    #model = BLog
    queryset = Blog.objects.all().order_by("-id")
    success_url = '/blog/list/'


class BlogDetailView(DetailView):
    template_name = 'blogdetail.html'
    model = Blog
    context_object_name = 'blogobject'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CommentForm

        return context


class BlogCommentView(CreateView):
    template_name = 'blogcomment.html'
    form_class = CommentForm
    success_url = '/blog/list/'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('/signin/')

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.commentor = self.request.user
        blog_id = self.kwargs["pk"]
        blog = Blog.objects.get(id=blog_id)
        form.instance.blog = blog
        return super().form_valid(form)

    def get_success_url(self):
        blog_id = self.kwargs['pk']
        url_to_redirect = '/blog/' + str(blog_id) + '/detail/'

        return url_to_redirect


class PasswordChangeView(FormView):
    template_name = 'passwordchange.html'
    form_class = PasswordChangeForm
    success_url = reverse_lazy('umapp:home')

    def form_valid(self, form):
        logged_in_user = self.request.user
        new_password = form.cleaned_data['new_password']
        old_password = form.cleaned_data['old_password']
        user = authenticate(username=logged_in_user.username,
                            password=old_password)
        if user is not None:
            logged_in_user.set_password(new_password)
            logged_in_user.save()
        else:

            return render(self.request, "passwordchange.html", {"error": 'old password is incorrect', "form": form})
